# Changelog n200403

This  nightly build has the next improvements and bugs fixed that differs from stable version:


## Improvements
* **Fixed windows install**, problem with python detection.
* **Python virtualenv ugrade**, to 16.6.10 version.
* **MacOSX Catalina and Mojave** support
* **Python 3.8.2** package for windows installs
* **Development mode**, for nightly versions.
* **Plugin system**, with serial terminal embeded as first plugin.
* **NEXTPNR** toolchain.
* **Improve compatibility**, with windows and MacOSX.
* **Upgrade core componentes** , this version has been upgraded with latests or closest latest versions of nwjs, and some js libraries.
* **Improve velocity** for larger designs (first round of optimizations).
* *****Submodule (block) edition** with *****copy & clone** or **copy & paste**
* *****Export submodule (block) as new file******
* Verilog verification at submodules (blocks)
* Support for **generic blocks** , very usefull for block edition
* New type of input/output block that permits use *****labels** instead of wires [issue 219](https://github.com/FPGAwars/icestudio/issues/219)
* Support for *****larger screens** of any size
* Auto check for new versions


## Bugs fixed
* Some Windows operating system bugs
* Icons to remove wires
* Labeling order on load project at inputs and outputs of all boards (fixed before only for Alhambra II board).
* Node 12 support [issue 329](https://github.com/FPGAwars/icestudio/issues/329)

